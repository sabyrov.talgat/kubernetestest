#!/bin/bash

if [ -z "$(kubectl get namespace | grep $K8S_NAMESPACE)" ];
then
  kubectl create namespace $K8S_NAMESPACE
  kubectl create secret docker-registry regsecret \
    --docker-server=$CI_REGISTRY \
    --docker-username=gitlab+deploy-token-3483 \
    --docker-email=none@none.com \
    --docker-password=$CI_DEPLOY_PASSWORD \
    --namespace=$K8S_NAMESPACE
fi

helm upgrade \
  --install \
  --set version=$IMAGE_VERSION \
  --set host=$APP_HOST \
  --set commit=$CI_COMMIT_SHA \
  --namespace $K8S_NAMESPACE \
  $K8S_NAMESPACE \
  devops/helm/monitoring-center
