#!/bin/bash

cd ../src/server/MonitoringCenter.WebApi && \
dotnet publish -c Release && \
docker build -t monitoring-center/web-api:0.1.0 . && \
cd ../../../devops

cd ../src/client && \
ng build --prod && \
docker build -t monitoring-center/web-app:0.1.0 . && \
cd ../../../devops

docker build -t registry.gitlab.com/drim-consulting/devops/monitoring-center/web-api:0.1.0 .
docker build -t registry.gitlab.com/drim-consulting/devops/monitoring-center/web-app:0.1.0 .
docker build -t registry.gitlab.com/drim-consulting/devops/monitoring-center/database:0.1.0 .