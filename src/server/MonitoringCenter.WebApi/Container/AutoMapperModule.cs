﻿using Autofac;
using AutoMapper;

namespace MonitoringCenter.WebApi.Container
{
    public class AutoMapperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Mapper.Initialize(config =>
            {
                config.AddProfiles(ThisAssembly);
            });

            builder
                .Register(ctx => Mapper.Instance)
                .As<IMapper>();
        }
    }
}