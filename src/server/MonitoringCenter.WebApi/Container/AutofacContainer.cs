﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using MonitoringCenter.DataAccess.Container;

namespace MonitoringCenter.WebApi.Container
{
    public static class AutofacContainer
    {
        public static IContainer Build(IServiceCollection existingServices)
        {
            var builder = new ContainerBuilder();

            builder.Populate(existingServices);

            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<AutoMapperModule>();

            return builder.Build();
        }
    }
}