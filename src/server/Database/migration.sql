CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" varchar(150) NOT NULL,
    "ProductVersion" varchar(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);

CREATE TABLE "Measurements" (
    "Id" bigserial NOT NULL,
    "TemperatureInCelsius" double precision NOT NULL,
    "HumidityPercent" double precision NOT NULL,
    "AtmospherePressure" double precision NOT NULL,
    CONSTRAINT "PK_Measurements" PRIMARY KEY ("Id")
);

INSERT INTO "Measurements" ("Id", "AtmospherePressure", "HumidityPercent", "TemperatureInCelsius")
VALUES (1, 741, 85.299999999999997, 22.100000000000001);

INSERT INTO "Measurements" ("Id", "AtmospherePressure", "HumidityPercent", "TemperatureInCelsius")
VALUES (2, 745, 79.400000000000006, 20);

INSERT INTO "Measurements" ("Id", "AtmospherePressure", "HumidityPercent", "TemperatureInCelsius")
VALUES (3, 740, 77.099999999999994, 21.899999999999999);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20180616120302_InitialCreate', '2.1.0-rtm-30799');
