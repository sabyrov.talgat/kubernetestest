﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MonitoringCenter.Configuration;

namespace MonitoringCenter.DataAccess.Container
{
    public class DataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .Register(c =>
                {
                    var configuration = c.Resolve<IConfigurationRoot>();

                    var optionsBuilder = new DbContextOptionsBuilder<MonitoringCenterDbContext>()
                        .UseNpgsql(configuration.GetValue<string>(KeyFor.PostgreSql.ConnectionString));

                    return new MonitoringCenterDbContext(optionsBuilder.Options);
                })
                .As<MonitoringCenterDbContext>()
                .InstancePerLifetimeScope();
        }
    }
}